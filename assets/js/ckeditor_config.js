CKEDITOR.editorConfig = function (config) {
    config.toolbar = [
        ['Link', 'Bold', 'Italic', 'BulletedList', 'Image']
    ];
    config.uiColor = '#FFFFFF';
    config.height = 100;
    config.resize_enabled = false;
    config.removePlugins = 'elementspath';
    config.toolbarLocation = 'bottom';
    config.extraPlugins='onchange,confighelper,menubutton,htmlbuttons';
};
