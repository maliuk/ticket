(function ($) {
    $(document).ready(function () {
        $('.control-menu a.dropdown-toggle').click(function () {
            $('.control-sidebar').removeClass('control-sidebar-open');
        });
        $('.control-menu .dropdown-menu a').click(function () {
            var link = $(this).attr('data-link');
            $('.control-sidebar-tabs a[href="#' + link + '"]').click();
        });
        $('.info-btn .btn').click(function () {
            var active = $(this).attr('data-active');
            $('.info-btn .btn').addClass('active');
            $(this).removeClass('active');
            $('.info-btn .label').removeClass('active');
            $('.label#' + active).addClass('active');
        });
        if ($('#editor_ticket').length) {
            CKEDITOR.replace('editor_ticket', {
                customConfig: '../../assets/js/ckeditor_config.js'
            });
            CKEDITOR.instances.editor_ticket.on("focus", function (event) {
                $('.estimate-box').removeClass('hidden');
            });
            // empty-textarea
            CKEDITOR.instances.editor_ticket.on("key", function (event) {
                var tc = CKEDITOR.instances.editor_ticket.getData().length;
                if (tc < 1) {
                    $('#editor_ticket').parent().addClass('empty-textarea');
                } else {
                    $('#editor_ticket').parent().removeClass('empty-textarea');
                }
            });
        }
        function formatState(state) {
            if (!state.id) {
                return state.text;
            }
            var baseUrl = "https://www.iconfinder.com/data/icons/stripe-flag-set/23";
            var $state = $(
                    '<span><img src="' + baseUrl + '/' + state.element.value + '.png" class="img-flag" /></span>'
                    );
            return $state;
        }
        ;

        $(".js-country-templating select").select2({
            templateSelection: formatState
        });
//        $('select').addClass('skin-select');
        $('.skin-select').select2();
        $('a[href="#"]').click(function (e) {
            e.preventDefault();
        });
        $('.skin-textarea').keydown(function () {
            var el = this;
            setTimeout(function () {
                el.style.cssText = 'height:auto; padding:0';
                // for box-sizing other than "content-box" use:
                // el.style.cssText = '-moz-box-sizing:content-box';
                el.style.cssText = 'height:' + el.scrollHeight + 'px';
            }, 0);
        });
        if ($('.details-box.full').hasClass('full')) {
            $(".details-box.full .skin-textarea").trigger("keydown");
            toastr.success('Updated Successfully!');
        }
        $('form').submit(function () {
            toastr.success('Updated Successfully!');
            return false;
        });
        $('.skin-input label').click(function () {
            if (!$(this).hasClass('active')) {
                $(this).addClass('active');
                $(this).parent().find('input').focus();
            }
        });
        $('.share_btn').click(function () {
            $('.share_btn').toggleClass('hide');
        });
        $('.share_input .btn').click(function () {
            document.querySelector('#share_link').select();
            document.execCommand('copy');
            toastr.success('Copied to clipboard!');
        });
        $('#add_members .hover_employee').click(function () {
            $(this).text('add employee');
            $(this).parent().appendTo('#members .chat');
            toastr.success('Employee remove successfully!');
        });
        $('#members .hover_employee').click(function () {
            $(this).text('remove employee');
            $(this).parent().appendTo('#add_members');
            $('.add_employees').removeClass('hidden');
            $('#members').removeClass('in');
            toastr.success('Employee add successfully!');
        });
        $('.add_employees').click(function () {
            $(this).addClass('hidden');
        });
        // demo
        $('.chart .btn').click(function () {
            $('.chart .btn').removeClass('btn-default');
            $(this).addClass('btn-default');
        });
        $('.bottom_menu a[href="#"]').click(function () {
            $('#modal-pages').modal('show');
            $('#modal-pages .modal-title').text('Page List');
        });
        function getPageName(url) {
            var index = url.lastIndexOf("/") + 1;
            var filenameWithExtension = url.substr(index);
            var filename = filenameWithExtension.split(".")[0];
            return filename;
        }
        var active, page = [
            'NewUser',
            'NewProject',
            'Project',
            'ProjectDetails',
            'Sorting',
            'Projectslist',
            'Statement',
            'ClientProfile',
            'TicketNew',
            'EmployeeStatistics',
            'EmployeeTicketviewtimer',
            'Timerfullscreen',
            'EmployeeProfile',
            'TicketEstimate',
            'ManagerClientlist',
            'ManagerProjectslist',
            'ManagerProjectTickets',
            'ManagerEmployeeStatistics',
            'ManagerTicketedit',
            'ManagerTicketmembers'
        ];
        for (var i = 0, l = page.length; i < l; i++) {
            if (page[i] == getPageName(document.location.pathname)) {
                active = 'active';
            } else {
                active = '';
            }
            $("#modal-pages .modal-body .list-group").append($('<a href="' + page[i] + '.html" class="list-group-item list-group-item-action ' + active + '">' + page[i] + '</a>'));
        }
        // demo chart
        if ($('#randomizeDay').length) {
            document.getElementById('randomizeDay').addEventListener('click', function () {
                barChartData.datasets.forEach(function (dataset) {
                    dataset.data = dataset.data.map(function () {
                        return randomScalingFactor();
                    })
                });
                window.myBar.update();
            });
        }
        if ($('#randomizeWeek').length) {
            document.getElementById('randomizeWeek').addEventListener('click', function () {
                barChartData.datasets.forEach(function (dataset) {
                    dataset.data = dataset.data.map(function () {
                        return randomScalingFactor();
                    })
                });
                window.myBar.update();
            });
        }
        if ($('#randomizeMonth').length) {
            document.getElementById('randomizeMonth').addEventListener('click', function () {
                barChartData.datasets.forEach(function (dataset) {
                    dataset.data = dataset.data.map(function () {
                        return randomScalingFactor();
                    })
                });
                window.myBar.update();
            });
        }
    });
})(jQuery);